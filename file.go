package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
)

func copyFile(src string, dst string) error {
	dstFile, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("failed to paste file: failed to create dst file: %s", err)
	}
	defer dstFile.Close()

	srcFile, err := os.OpenFile(src, os.O_RDONLY, 0655)
	if err != nil {
		return fmt.Errorf("failed to paste file: failed to read src file: %s", err)
	}
	defer srcFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		return err
	}

	return nil
}

func copyRecursive(src string, dst string) error {
	var err error
	var fds []os.FileInfo
	var srcinfo os.FileInfo

	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}

	if srcinfo.Mode()&os.ModeDir != 0 {
		if err = os.MkdirAll(dst, srcinfo.Mode()); err != nil {
			return err
		}
		if fds, err = ioutil.ReadDir(src); err != nil {
			return err
		}
	} else {
		fds = append(fds, srcinfo)
	}

	for _, fd := range fds {
		srcfp := path.Join(src, fd.Name())
		dstfp := path.Join(dst, fd.Name())

		if fd.IsDir() {
			if err = copyRecursive(srcfp, dstfp); err != nil {
				return err
			}
		} else {
			if err = copyFile(srcfp, dstfp); err != nil {
				return err
			}
		}
	}
	return nil
}
