# adbfm
[![CI status](https://gitlab.com/tslocum/adbfm/badges/master/pipeline.svg)](https://gitlab.com/tslocum/adbfm/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

ADB file manager

## Missing features

This application is missing a few features before its initial release:

- Recursive upload/download (only single file upload/download is currently supported)
- Display a confirmation dialog before overwriting a file

## Download

adbfm is written in [Go](https://golang.org). Run the following command to
download and build adbfm from source.

```bash
go get gitlab.com/tslocum/adbfm
```

The resulting binary is available as `~/go/bin/adbfm`.

## Usage

adbfm may be used with a keyboard or mouse.

- Navigate: Arrow keys or VIM keys (H/J/K/L), Tab, Shift+Tab
- Open context menu: Alt+Enter
- Enter directory: Enter

Launch with `--connect` to connect to a device over a network connection.

```bash
adb --connect=192.168.8.100
```

## Support

Please share issues and suggestions [here](https://gitlab.com/tslocum/adbfm/issues).

## Dependencies

* [zach-klippenstein/goadb](https://github.com/zach-klippenstein/goadb)
