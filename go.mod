module gitlab.com/tslocum/adbfm

go 1.15

require (
	github.com/gdamore/tcell/v2 v2.0.1-0.20201109052606-7d87d8188c8d
	github.com/mattn/go-runewidth v0.0.9
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/zach-klippenstein/goadb v0.0.0-20201208042340-620e0e950ed7
	gitlab.com/tslocum/cbind v0.1.4
	gitlab.com/tslocum/cview v1.5.2
	golang.org/x/sys v0.0.0-20201207223542-d4d67f95c62d // indirect
)
